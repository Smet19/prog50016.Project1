# Spaceship Game by Smetankin Mikhail

SFML/C++ Game project for Game Architecture course

## Description

Top-dow spaceship shooting game made with C++ and SFML for Game Architecture course. Assignment brief can be found in [Project#1.pdf](https://gitlab.com/Smet19/prog50016.Project1/-/blob/main/Project%231.pdf) file.

**Rest of the README file was part of the assignment.**

# Planning
My initial plan was to complete all Basic Requirements by Tuesday, November 2, and to do each requirement on each day. 
But I started my project by implementing GameEngine classes from Lab 2. 
That was a mistake. Structure of classes of GameEngine is too complex for this Project. 
I had to redo everything and cleanup entire solution. 
As of Nov 4th 12:00 pm I’m done with the project for about 85 percent. I won’t do Persistence because I don’t have time to do that. I need to finish my Game Engine Mid-term Project.

# Data Setup

All data is stored in project folder/Settings . I use JSON files to store all the loadable data.

GameSettings.json contains general settings of the game as well as filenames of game entities that can be loaded (player ship, enemy ships, asteroids).

For those entities each .json file has similar structure, bar asteroids. Asteroids files don't contain “projectile Texture” parameter, because asteroids don't use projectiles.

# Incomplete (vs what was asked)

As stated in Planning chapter, Persistence part isn’t complete because I don’t have time for that.

There’s also a visual bug with projectiles and asteroids. While it doesn’t affect gameplay, it’s still a bug. I don’t have time to fix that.

**Statistics part is also incomplete, I will update my project after I finish my Game Engine Mid-term project**

# Screenshots

Can't post screenshots in ReadMe. You have to check the document on Slate
