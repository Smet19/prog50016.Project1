#pragma once
#ifndef _ASSETMANAGER_H_
#define	_ASSETMANAGER_H_

#include <string>
#include <iostream>
#include <map>
#include <SFML/Graphics.hpp>
#include "Initializable.h"
#include "Singleton.h"
#include "Initializable.h"

// Handles and stores all textures that are loaded while the game runs
class AssetManager : public Singleton<AssetManager>, public Initializable
{
public:
	// Constructors / Destructors
	AssetManager();
	virtual ~AssetManager();

	// Methods
	void initialize() override;
	sf::Texture* getTexture(std::string _fileName); // Get texture by file path.

private:
	// Stores textures
	std::map<std::string, sf::Texture*> textureMap;
};

#endif // !_ASSETMANAGER_H_

