#include "BackgroundManager.h"
#include "SettingsManager.h"
#include "AssetManager.h"
#include "GameManager.h"
#include <random>

BackgroundManager::BackgroundManager()
{
	backgroundColor = "./Assets/default.png";
	nebula = "./Assets/default.png";
	starBig = "./Assets/default.png";
	starSmall = "./Assets/default.png";
	speedLine = "./Assets/default.png";
	speed = 1.0f;
}
BackgroundManager::~BackgroundManager()
{
	for (auto* sprite : sprites)
	{
		delete sprite;
	}
}

// Loading background images info
void BackgroundManager::load(json::JSON& _json)
{
	if (!loaded)
	{
		if (_json.hasKey("backgroundColor"))
		{
			backgroundColor = _json["backgroundColor"].ToString();
		}

		if (_json.hasKey("nebula"))
		{
			nebula = _json["nebula"].ToString();
		}

		if (_json.hasKey("starBig"))
		{
			starBig = _json["starBig"].ToString();
		}

		if (_json.hasKey("starSmall"))
		{
			starSmall = _json["starSmall"].ToString();
		}

		if (_json.hasKey("speedLine"))
		{
			speedLine = _json["speedLine"].ToString();
		}

		if (_json.hasKey("speed"))
		{
			speed = _json["speed"].ToFloat();
		}

		loaded = true;
	}
}

void BackgroundManager::initialize()
{
	if (!initialized && loaded)
	{
		//First we load background image
		const sf::RenderWindow* window = GameManager::getInstance().getWindow();
		sf::Texture* texture = AssetManager::getInstance().getTexture(backgroundColor);
		background.setTexture(*texture);
		background.setOrigin(texture->getSize().x * 0.5, texture->getSize().y * 0.5);
		background.setScale(window->getSize().x / texture->getSize().x + 1, window->getSize().y / texture->getSize().y + 1);
		background.setPosition(window->getSize().x * 0.5f, window->getSize().y * 0.5f);

		// Then we randomly create one of the moving parts
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> spawn(1, 100);

		for (int i = 0; i < 20; i++)
		{
			sf::Sprite* sprite = new sf::Sprite();
			int spawnChance = spawn(gen);

			if (spawnChance > 85)
			{
				texture = AssetManager::getInstance().getTexture(nebula);
				sprite->setTexture(*texture);
			}
			else if (spawnChance > 50)
			{
				texture = AssetManager::getInstance().getTexture(starBig);
				sprite->setTexture(*texture);
			}
			else if (spawnChance > 15)
			{
				texture = AssetManager::getInstance().getTexture(starSmall);
				sprite->setTexture(*texture);
			}
			else
			{
				texture = AssetManager::getInstance().getTexture(speedLine);
				sprite->setTexture(*texture);
			}
			// After we picked our sprite - place it on a random coordinate
			sprite->setOrigin(texture->getSize().x * 0.5, texture->getSize().y * 0.5);

			std::uniform_real_distribution<> posX(texture->getSize().x, window->getSize().x - texture->getSize().x);
			std::uniform_real_distribution<> posY(texture->getSize().y, window->getSize().y - texture->getSize().y);
			sprite->setPosition(posX(gen), posY(gen));

			sprites.push_back(sprite);
		}

		initialized = true;
	}
}

void BackgroundManager::update()
{
	const sf::RenderWindow* window = GameManager::getInstance().getWindow();

	std::random_device rd;
	std::mt19937 gen(rd());

	for (auto* sprite : sprites)
	{
		std::uniform_int_distribution<> posX(sprite->getTexture()->getSize().x, window->getSize().x - sprite->getTexture()->getSize().x);

		// Move our sprite
		sprite->move(0, speed);

		// If it's offscreen - put it at the start of the screen on random X position

		if (sprite->getPosition().y >= window->getSize().y + sprite->getTexture()->getSize().y)
		{
			//But also a little bit off the screen so it appears smoothly
			sprite->setPosition(posX(gen), -1.0f * sprite->getTexture()->getSize().y);
			sprite->move(0, speed);
		}
	}
}