#pragma once
#ifndef _BACKGROUND_MANAGER_H_
#define _BACKGROUND_MANAGER_H_

#include "Loadable.h"
#include "Singleton.h"
#include "Initializable.h"
#include <SFML/Graphics.hpp>
#include <list>
#include <string>

// Handles background of the game, making it dynamic
class BackgroundManager : public Singleton<BackgroundManager>, public Loadable, public Initializable
{
public:
	BackgroundManager();
	virtual ~BackgroundManager();

	void update();
	virtual void load(json::JSON& _json) override;
	virtual void initialize() override;

	const std::list<sf::Sprite*>& getSprites() { return sprites; }
	const sf::Sprite& getBackground() { return background; }

private:
	std::string backgroundColor;
	std::string nebula;
	std::string starBig;
	std::string starSmall;
	std::string speedLine;

	float speed;

	sf::Sprite background;
	std::list<sf::Sprite*> sprites;
};


#endif // !_BACKGROUND_MANAGER_H_