#include "DatabaseManager.h"

sqlite3* DatabaseManager::Open()
{
	sqlite3* db = nullptr;

	int result = sqlite3_open("Example.db", &db);
	if (result)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
		return nullptr;
	}

	std::cout << "Opened database successfully" << std::endl;
	return db;
}

void DatabaseManager::Create()
{
	// Open DB
	sqlite3* db = Open();
	assert(db != nullptr, "Unable to open database");

	std::string name;
	std::cout << "Enter Name: ";
	std::cin >> name;

	std::string statement = "INSERT INTO Example_Table (name) VALUES (@name);";

	sqlite3_stmt* stm;
	int result = sqlite3_prepare_v2(db, statement.c_str(), -1, &stm, 0);
	if (result == SQLITE_OK)
	{
		int index = sqlite3_bind_parameter_index(stm, "@name");
		sqlite3_bind_text(stm, index, name.c_str(), name.length(), nullptr);

		result = sqlite3_step(stm);
		sqlite3_finalize(stm);
		if (result != SQLITE_DONE)
		{
			// error
		}
	}
	else
	{
		std::cout << "Failed to prepare statement: " << sqlite3_errmsg(db) << std::endl;
	}

	sqlite3_close(db);
}

int ReadCallback(void* data, int argc, char** argv, char** azColName)
{
	// data is what we pass in, this case it is a string (it coule be an object)
	std::cout << (const char*)data << std::endl;
	for (int i = 0; i < argc; i++)
	{
		std::cout << azColName[i] << " " << argv[i] << std::endl;
	}
	std::cout << "----------" << std::endl;
	return 0;
}

void DatabaseManager::Read()
{
	char* errMsg = nullptr;

	// Open DB
	sqlite3* db = Open();
	assert(db != nullptr, "Unable to open database");

	std::string statement = "SELECT * FROM Example_Table";
	int result = sqlite3_exec(db, statement.c_str(), ReadCallback, (void*)"READ_*", &errMsg);
	if (result != SQLITE_OK)
	{
		std::cout << "Error executing read: " << errMsg << std::endl;
		sqlite3_free(errMsg);
	}

	sqlite3_close(db);
}

void DatabaseManager::Update()
{
	// Open DB
	sqlite3* db = Open();
	assert(db != nullptr, "Unable to open database");

	int id = -1;
	std::cout << "Enter id to update: ";
	std::cin >> id;

	std::string name;
	std::cout << "Enter new Name: ";
	std::cin >> name;

	std::string sql_statement = "UPDATE Example_Table SET name = @name WHERE id = @id;";
	sqlite3_stmt* statement = nullptr;
	int result = sqlite3_prepare_v2(db, sql_statement.c_str(), -1, &statement, nullptr);
	if (result == SQLITE_OK)
	{
		int index = sqlite3_bind_parameter_index(statement, "@id");
		sqlite3_bind_int(statement, index, id);

		index = sqlite3_bind_parameter_index(statement, "@name");
		sqlite3_bind_text(statement, index, name.c_str(), name.length(), nullptr);

		result = sqlite3_step(statement);
		sqlite3_finalize(statement);

		// can check the result
	}
	// else check

	sqlite3_close(db);
}