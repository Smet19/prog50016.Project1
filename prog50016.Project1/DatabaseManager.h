#pragma once
#ifndef _DATABASE_MANAGER_H_
#define _DATABASE_MANAGER_H_

#include <iostream>
#include <cassert>
#include "sqlite3.h"

// Manages our data. Incomplete
class DatabaseManager
{
private:
	sqlite3* Open();
	void Create();
	void Read();
	void Update();
};

#endif // !_DATABASE_MANAGER_H_
