#pragma once
#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_

#include "Initializable.h"
#include <SFML/Graphics.hpp>


// Interface that handles work with sprites
class Drawable
{
public:
	Drawable();
	virtual ~Drawable();

	virtual void draw(sf::RenderWindow* _window) { _window->draw(sprite); }

	// set's texture of object's sprite
	void setTexture(sf::Texture* _texture);
	// returns pointer to object's sprite
	const sf::Sprite* getSprite() { return &sprite; }

	// Move object by offset
	void Move(float x, float y) { sprite.move(x, y); }
	void Move(sf::Vector2f& _offset) { sprite.move(_offset); }
	// Set specific position
	void SetPosition(float x, float y) { sprite.setPosition(x, y); }
	void SetPosition(sf::Vector2f& _pos) { sprite.setPosition(_pos); }

	// Get position
	const sf::Vector2f& GetPosition() { return sprite.getPosition(); };

	float getDistanceTo(Drawable* _distanceTo);

protected:
	sf::Sprite sprite;

};
#endif // !_DRAWABLE_H_