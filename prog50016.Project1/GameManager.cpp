#include "GameManager.h"
#include "SettingsManager.h"
#include "AssetManager.h"
#include "BackgroundManager.h"
#include <random>

GameManager::GameManager()
{
	highestScore = 100;
	gameState = GameState::Setup;
	window = nullptr;
}

GameManager::~GameManager()
{
	for (auto* ship : enemies)
	{
		delete ship;
	}

	for (auto* ast : asteroids)
	{
		delete ast;
	}

	delete window;
}

void GameManager::initialize()
{
	if (!initialized)
	{
		SettingsManager::getInstance().initialize();

		// Loading UI font
		if (!uiFont.loadFromFile(SettingsManager::getInstance().UIFontPath()))
		{
			return;
		}	

		AssetManager::getInstance().initialize();

		// Checking if we can get background settings
		if (SettingsManager::getInstance().isInitialized())
		{
			BackgroundManager::getInstance().load(SettingsManager::getInstance().getBackgroundSettings());
		}


		// Creating new window 
		window = new sf::RenderWindow(sf::VideoMode(
			SettingsManager::getInstance().Width(),
			SettingsManager::getInstance().Height()),
			SettingsManager::getInstance().WindowName(),
			SettingsManager::getInstance().Fullscreen()
			? sf::Style::Titlebar | sf::Style::Close | sf::Style::Fullscreen
			: sf::Style::Titlebar | sf::Style::Close);

		window->setFramerateLimit(SettingsManager::getInstance().FrameLimit());

		BackgroundManager::getInstance().initialize();

		// Initializing  UI
		score.setFont(uiFont);
		score.setCharacterSize(18);
		score.setStyle(sf::Text::Regular);
		score.setFillColor(sf::Color::White);
		score.setString("Score: ");
		score.setOrigin(score.getLocalBounds().width * 0.5f, score.getLocalBounds().height * 0.5f);
		score.setPosition(score.getLocalBounds().width, score.getLocalBounds().height);

		health.setFont(uiFont);
		health.setCharacterSize(18);
		health.setStyle(sf::Text::Regular);
		health.setFillColor(sf::Color::White);
		health.setString("Health: ");
		health.setOrigin(score.getLocalBounds().width * 0.5f, score.getLocalBounds().height * 0.5f);
		health.setPosition(score.getPosition().x + score.getLocalBounds().width + health.getLocalBounds().width, 
			health.getLocalBounds().height);

		highScore.setFont(uiFont);
		highScore.setCharacterSize(18);
		highScore.setStyle(sf::Text::Regular);
		highScore.setFillColor(sf::Color::White);
		highScore.setString("High Score: ");
		highScore.setOrigin(score.getLocalBounds().width * 0.5f, score.getLocalBounds().height * 0.5f);
		highScore.setPosition(health.getPosition().x + health.getLocalBounds().width + highScore.getLocalBounds().width,
			highScore.getLocalBounds().height);


		// Loading player, placing it at starting point
		player.load(SettingsManager::getInstance().getPlayerSettings());
		player.SetPosition(window->getSize().x * 0.5f, window->getSize().y - player.getSprite()->getTexture()->getSize().y - 10);

		updateUI();

		initialized = true;
	}
}

// Restart our game
void GameManager::restart()
{
	for (auto* ship : enemies)
	{
		delete ship;
	}
	enemies.clear();

	for (auto* ast : asteroids)
	{
		delete ast;
	}
	asteroids.clear();

	while(enemies.size() < 2)
	{
		createEnemy();
	}

	while (asteroids.size() < 4)
	{
		createAsteroid();
	}

	// Creating a new player
	player = PlayerShip();
	player.load(SettingsManager::getInstance().getPlayerSettings());
	player.SetPosition(window->getSize().x * 0.5f, window->getSize().y - player.getSprite()->getTexture()->getSize().y - 10);

	updateUI();
}

// Update our Score, Helath and Highscore
void GameManager::updateUI()
{
	std::string buffer;

	buffer = "Score: ";
	buffer += std::to_string(player.points);

	score.setString(buffer);
	score.setPosition(score.getLocalBounds().width, score.getLocalBounds().height);

	buffer = "Health: ";
	buffer += std::to_string(player.lives);

	health.setString(buffer);
	health.setPosition(score.getPosition().x + score.getLocalBounds().width + health.getLocalBounds().width,
		health.getLocalBounds().height);

	buffer = "High Score: ";
	buffer += std::to_string(highestScore);

	highScore.setString(buffer);
	highScore.setPosition(health.getPosition().x + health.getLocalBounds().width + highScore.getLocalBounds().width,
		highScore.getLocalBounds().height);
}

void GameManager::update()
{
	sf::Event event;
	window->clear();

	while (window != nullptr && window->pollEvent(event))
	{
		// Polling events first
		if (event.type == sf::Event::LostFocus)
		{
			break;
		}
		switch (event.type)
		{
		case sf::Event::Closed:
			GameManager::gameState = GameState::GameEnded;
			window->close();
			delete window;
			window = nullptr;
			break;

		case sf::Event::KeyPressed:
			switch (event.key.code)
			{
			// Escape - Exit the game
			case sf::Keyboard::Escape:
				GameManager::gameState = GameState::GameEnded;
				window->close();
				delete window;
				window = nullptr;
				break;

			// Space - Shoot
			case sf::Keyboard::Space:
				player.shoot();
				break;
			}
			break;
		}
	}

	if (GameManager::getInstance().gameState == GameState::Gameplay)
	{
		// Starting with background
		BackgroundManager::getInstance().update();

		player.update();

		//Updating all enemies and asteroids
		for (auto* ship : enemies)
		{
			ship->update();
		}

		for (auto* ast : asteroids)
		{
			ast->update();
		}

		// Creating new enemies and asteroids
		if (enemies.size() < 2)
		{
			createEnemy();
		}

		if (asteroids.size() < 4)
		{
			createAsteroid();
		}


		// Player movement
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			player.Move(0.0f, -player.speed);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			player.Move(0, player.speed);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			player.Move(player.speed, 0);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			player.Move(-player.speed, 0);
		}
	}


	// After we finished updating everything - we must draw objects on screen
	if (window != nullptr)
	{
		//Starting with background
		window->draw(BackgroundManager::getInstance().getBackground());

		// And it's moving parts
		for (auto* sprite : BackgroundManager::getInstance().getSprites())
		{
			window->draw(*sprite);
		}

		//Then player
		window->draw(*player.getSprite());

		// And it's fired projectiles
		for (auto* projectile : player.getProjectiles())
		{
			window->draw(*projectile->getSprite());
		}

		// Then enemies
		for (auto* enemy : enemies)
		{
			//Drawing enemy
			window->draw(*enemy->getSprite());
			//And it's projectile
			if(enemy->projectile != nullptr && enemy->projectile->getSprite() != nullptr)
				window->draw(*enemy->projectile->getSprite());
		}
		// And asteroids
		for (auto* ast : asteroids)
		{
			// Drawing asteroid
			window->draw(*ast->getSprite());
			// No projectile - no drawing;
		}

		// UI
		window->draw(health);
		window->draw(score);
		window->draw(highScore);

		window->display();
	}
}

// Creating enemy
void GameManager::createEnemy()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 2); // 33 percent chance of spawning each individual type of enemy


	if (dis(gen) == 1)
	{
		// Creating new enemy
		EnemyShipB* enemy = new EnemyShipB();
		if (SettingsManager::getInstance().isInitialized())
		{
			// Loading data
			enemy->load(SettingsManager::getInstance().getEnemySettingsB());

			// Generating random position
			std::uniform_real_distribution<> posX(enemy->getSprite()->getTexture()->getSize().x, 
				window->getSize().x - enemy->getSprite()->getTexture()->getSize().x);

			std::uniform_real_distribution<> posY(enemy->getSprite()->getTexture()->getSize().y,
				window->getSize().y * 0.5f - enemy->getSprite()->getTexture()->getSize().y);

			// Assigning random position
			enemy->SetPosition(posX(gen), posY(gen));
			addEnemy(enemy);
		}	
	}
	else if (dis(gen) == 2)
	{
		EnemyShip* enemy = new EnemyShip();
		if (SettingsManager::getInstance().isInitialized())
		{
			// Loading data
			enemy->load(SettingsManager::getInstance().getEnemySettings());

			// Generating random position
			std::uniform_real_distribution<> posX(enemy->getSprite()->getTexture()->getSize().x,
				window->getSize().x - enemy->getSprite()->getTexture()->getSize().x);

			std::uniform_real_distribution<> posY(enemy->getSprite()->getTexture()->getSize().y,
				window->getSize().y * 0.5f - enemy->getSprite()->getTexture()->getSize().y);

			// Assigning random position
			enemy->SetPosition(posX(gen), posY(gen));
			addEnemy(enemy);
		}
	}
	else
	{
		EnemyShipC* enemy = new EnemyShipC();
		if (SettingsManager::getInstance().isInitialized())
		{
			// Loading data
			enemy->load(SettingsManager::getInstance().getEnemySettingsC());

			// Generating random position
			std::uniform_real_distribution<> posX(enemy->getSprite()->getTexture()->getSize().x,
				window->getSize().x - enemy->getSprite()->getTexture()->getSize().x);

			std::uniform_real_distribution<> posY(enemy->getSprite()->getTexture()->getSize().y,
				window->getSize().y * 0.5f - enemy->getSprite()->getTexture()->getSize().y);

			// Assigning random position
			enemy->SetPosition(posX(gen), posY(gen));
			addEnemy(enemy);
		}
	}
}

// Generating asteroid
void GameManager::createAsteroid()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 2); 

	// Since I don't need to create class for each type of asteroid I can just load all the data and they still will act diffirently
	Asteroid* ast = new Asteroid();
	if (SettingsManager::getInstance().isInitialized())
	{
		// 1 out of 3 chance to spawn one type of asteroid
		if (dis(gen) == 0)
		{
			ast->load(SettingsManager::getInstance().getAsteroidSettingsA());
		}
		// 2 out of 3 chance to spawn other type
		else
		{
			ast->load(SettingsManager::getInstance().getAsteroidSettingsB());
		}

		// Spawn new asteroid at random X position
		std::uniform_real_distribution<> posX(ast->getSprite()->getTexture()->getSize().x,
			window->getSize().x - ast->getSprite()->getTexture()->getSize().x);

		ast->SetPosition(posX(gen), 0);
		
		addAsteroid(ast);
	}
}