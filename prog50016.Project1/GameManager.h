#pragma once
#ifndef _GAME_MANAGER_H_
#define _GAME_MANAGER_H_

#include "Singleton.h"
#include "Ships.h"
#include "Loadable.h"
#include "Initializable.h"
#include <list>

enum class GameState
{
	Setup,
	Gameplay,
	GameEnded
};

// Handles gameplay and game objects
class GameManager: public Singleton<GameManager>, public Initializable
{
public:
	GameManager();
	virtual ~GameManager();

	// Enemies
	const std::list<EnemyShip*>& getEnemies() { return enemies; }

	void addEnemy(EnemyShip* _enemy) { enemies.push_back(_enemy); }
	void removeEnemy(EnemyShip* _enemy) { enemies.remove(_enemy); }

	// Asteroids

	const std::list<Asteroid*>& getAsteroids() { return asteroids; }

	void addAsteroid(Asteroid* _ast) { asteroids.push_back(_ast); }
	void removeAsteroid(Asteroid* _ast) { asteroids.remove(_ast); }

	// Player

	PlayerShip* getPlayer() { return &player; }

	const sf::RenderWindow* getWindow() { return window; }

	void initialize() override;
	void update();
	void updateUI();
	void restart();

	int highestScore;
	GameState gameState;
private:
	void createEnemy();
	void createAsteroid();

	// Characters
	PlayerShip player;
	std::list<EnemyShip*> enemies;
	std::list<Asteroid*> asteroids;


	sf::RenderWindow* window;

	// UI

	sf::Font uiFont;
	sf::Text score;
	sf::Text health;
	sf::Text highScore;
};

#endif // _GAME_MANAGER_H_