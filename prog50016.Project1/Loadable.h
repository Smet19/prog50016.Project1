#pragma once
#ifndef _LOADABLE_H_
#define _LOADABLE_H_

#include "json.hpp"

class Loadable
{
public:
	virtual void load(json::JSON& _json) = 0;
	bool isLoaded() { return loaded; }

protected:
	bool loaded;
};
#endif // !_LOADABLE_H_