#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "Initializable.h"
#include "Loadable.h"
#include "Drawable.h"

class Object : public Loadable, public Drawable
{
public:
	// Constructors / Destructors
	Object() {};
	virtual ~Object() {};

	virtual void update() = 0;
};
#endif // !_OBJECT_H_

