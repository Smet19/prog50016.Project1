#pragma once
#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "Object.h"

class EnemyShip;
class PlayerShip;

class Projectile : public Object
{
public:
	Projectile() { speed = 5.0f; doDelete = false; }
	virtual ~Projectile() {};

	virtual void load(json::JSON& _json) override;

	bool doDelete;

protected:
	float speed;
};

class EnemyProjectile : public Projectile
{
public:
	EnemyProjectile() { owner = nullptr; }
	virtual ~EnemyProjectile() {};

	virtual void update() override;

	EnemyShip* owner;
	
};

class PlayerProjectile : public Projectile
{
public:
	PlayerProjectile() { owner = nullptr; }
	virtual ~PlayerProjectile() {};

	virtual void update() override;

	PlayerShip* owner;
};

#endif // !_PROJECTILE_H_

