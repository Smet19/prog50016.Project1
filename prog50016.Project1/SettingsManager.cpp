#include "SettingsManager.h"

SettingsManager::SettingsManager()
{
	windowName = "Default";
	width = 640;
	height = 480;
	frameLimit = 60;
	fullscreen = false;
}

SettingsManager::~SettingsManager()
{
}

void SettingsManager::initialize()
{
	if (!initialized)
	{
		std::ifstream inputStream("./Settings/GameSettings.json");
		std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
		settings = json::JSON::Load(str);


		if (settings.hasKey("windowName"))
		{
			windowName = settings["windowName"].ToString();
		}

		if (settings.hasKey("width"))
		{
			width = settings["width"].ToInt();
		}

		if (settings.hasKey("height"))
		{
			height = settings["height"].ToInt();
		}

		if (settings.hasKey("frameLimit"))
		{
			frameLimit = settings["frameLimit"].ToInt();
		}

		if (settings.hasKey("uiFontPath"))
		{
			uiFontPath = settings["uiFontPath"].ToString();
		}


		if (settings.hasKey("fullscreen"))
		{
			fullscreen = settings["fullscreen"].ToBool();
		}

		if (settings.hasKey("playerShip"))
		{
			std::ifstream inputStream(settings["playerShip"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			playerShip = json::JSON::Load(str);
		}

		if (settings.hasKey("enemyShip"))
		{
			std::ifstream inputStream(settings["enemyShip"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			enemyShip = json::JSON::Load(str);
		}

		if (settings.hasKey("enemyShipB"))
		{
			std::ifstream inputStream(settings["enemyShipB"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			enemyShipB = json::JSON::Load(str);
		}

		if (settings.hasKey("enemyShipC"))
		{
			std::ifstream inputStream(settings["enemyShipC"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			enemyShipC = json::JSON::Load(str);
		}

		if (settings.hasKey("background"))
		{
			std::ifstream inputStream(settings["background"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			background = json::JSON::Load(str);
		}

		if (settings.hasKey("asteroidA"))
		{
			std::ifstream inputStream(settings["asteroidA"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			asteroidA = json::JSON::Load(str);
		}

		if (settings.hasKey("asteroidB"))
		{
			std::ifstream inputStream(settings["asteroidB"].ToString());
			std::string str((std::istreambuf_iterator<char>(inputStream)), std::istreambuf_iterator<char>());
			asteroidB = json::JSON::Load(str);
		}

		initialized = true;
	}
}
