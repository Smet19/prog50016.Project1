#pragma once
#ifndef _SETTINGS_MANAGER_H_
#define _SETTINGS_MANAGER_H_

#include <iostream>
#include <fstream>
#include "Singleton.h"
#include "Initializable.h"
#include "json.hpp"

class SettingsManager : public Singleton<SettingsManager> , public Initializable
{
public:
	SettingsManager();
	virtual ~SettingsManager();

	void initialize() override;
	json::JSON& getSettings() { return settings; }

	json::JSON& getPlayerSettings() { return playerShip; }
	json::JSON& getEnemySettings() { return enemyShip; }
	json::JSON& getEnemySettingsB() { return enemyShipB; }
	json::JSON& getEnemySettingsC() { return enemyShipC; }
	json::JSON& getBackgroundSettings() { return background; }
	json::JSON& getAsteroidSettingsA() { return asteroidA; }
	json::JSON& getAsteroidSettingsB() { return asteroidB; }

	std::string WindowName() { return windowName; }
	int Width() { return width; }
	int Height() { return height; }
	int FrameLimit() { return frameLimit; }
	bool Fullscreen() { return fullscreen; }
	std::string UIFontPath() { return uiFontPath; }

private:
	json::JSON settings;

	std::string windowName;
	int width;
	int height;
	int frameLimit;
	bool fullscreen;
	std::string uiFontPath;

	json::JSON playerShip;
	json::JSON enemyShip;
	json::JSON enemyShipB;
	json::JSON enemyShipC;
	json::JSON background;
	json::JSON asteroidA;
	json::JSON asteroidB;
};

#endif // !_SETTINGS_MANAGER_H_

