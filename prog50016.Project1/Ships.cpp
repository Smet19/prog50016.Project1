#include "Ships.h"
#include "AssetManager.h"
#include "GameManager.h"
#include <random>


// Base class
Ship::Ship()
{
	status = ShipState::None;
	lives = 1;
	points = 0;
	speed = 0.1f;
	texturePath = "./Assets/default.png";
	projectileTexture = "./Assets/default.png";
	sf::Texture* texture = new sf::Texture();
	texture->loadFromFile(texturePath);
}

Ship::~Ship()
{
}

void Ship::takeDamage()
{
	if (status == ShipState::Alive && lives > 0)
	{
		lives--;
		// Checking if ship should be dead
		if (lives <= 0)
		{
			// Making him dead
			status = ShipState::Dead;
			onDeath();
		}
	}
}

// No comments
void Ship::die()
{
	if (status == ShipState::Alive && lives > 0)
	{
		lives = 0;
		status = ShipState::Dead;
		onDeath();
	}
}

// Loading data
void Ship::load(json::JSON& _json)
{
	if (_json.hasKey("lives"))
	{
		lives = _json["lives"].ToInt();
		if (lives > 0) status = ShipState::Alive;
	}

	if (_json.hasKey("points"))
	{
		points = _json["points"].ToInt();
	}

	if (_json.hasKey("speed"))
	{
		speed = (float)_json["speed"].ToFloat();
	}

	if (_json.hasKey("texturePath"))
	{
		texturePath = _json["texturePath"].ToString();
		setTexture(AssetManager::getInstance().getTexture(texturePath));
	}

	if (_json.hasKey("projectileTexture"))
	{
		projectileTexture = _json["projectileTexture"].ToString();
	}
}

// Enemies

// Basic one
EnemyShip::EnemyShip()
{
	direction = EnemyMoveDirection::Right;
	projectile = nullptr;
}

EnemyShip::~EnemyShip()
{

}

void EnemyShip::update()
{
	movement();
	shoot();
}


// 1 active projectile per enemy ship
void EnemyShip::shoot()
{
	if (projectile == nullptr)
	{
		// No projectile - create a new one
		projectile = new EnemyProjectile();
		projectile->owner = this;
		projectile->setTexture(AssetManager::getInstance().getTexture(projectileTexture));
		projectile->SetPosition(GetPosition().x, GetPosition().y + projectile->getSprite()->getTexture()->getSize().y);
	}
	else
	{
		// Projectile exists - update it;
		projectile->update();

		// If it's still exist - check if we hitting player
		if (projectile != nullptr)
		{
			PlayerShip* player = GameManager::getInstance().getPlayer();
			if (projectile->getDistanceTo(player) < projectile->getSprite()->getTexture()->getSize().y)
			{
				//If we do - player takes damage, projectile get's destroyed
				player->takeDamage();
				delete projectile;
				projectile = nullptr;
			}
		}
	}
}

void EnemyShip::movement()
{
	//Standart ship moves just left/right
	switch (direction)
	{
	case EnemyMoveDirection::Right:
		Move(speed, 0);
		if (GetPosition().x >= GameManager::getInstance().getWindow()->getSize().x - getSprite()->getTexture()->getSize().x)
			direction = EnemyMoveDirection::Left;
		break;
	case EnemyMoveDirection::Left:
		Move(-speed, 0);
		if (GetPosition().x <= getSprite()->getTexture()->getSize().x)
			direction = EnemyMoveDirection::Right;
		break;
	}
}

void EnemyShip::onDeath()
{
	// Check if ship is stored in GameManager
	if (std::find(GameManager::getInstance().getEnemies().begin(),
		GameManager::getInstance().getEnemies().end(), this)
		!= GameManager::getInstance().getEnemies().end())
	{
		GameManager::getInstance().removeEnemy(this);
	}

	// Player gets points
	GameManager::getInstance().getPlayer()->points += points;

	//TODO Play Animation

	// Updating scores
	GameManager::getInstance().updateUI();

	delete this;
}

void EnemyShipB::movement()
{
	// This ship tries to follow a player on X axis
	float dif = this->GetPosition().x - GameManager::getInstance().getPlayer()->GetPosition().x;
	if (dif > 10)
	{
		Move(-speed, 0);
	}
	else if (dif < 0)
	{
		Move(speed, 0);
	}
}

// Asteroids (1)

void Asteroid::update()
{
	// Asteroids don't shoot
	movement();
}

void Asteroid::onDeath()
{
	// Checking if object is in GameManager
	if (std::find(GameManager::getInstance().getAsteroids().begin(),
		GameManager::getInstance().getAsteroids().end(), this)
		!= GameManager::getInstance().getAsteroids().end())
	{
		GameManager::getInstance().removeAsteroid(this);
	}

	// Awarding points
	GameManager::getInstance().getPlayer()->points += points;

	//TODO Play Animation

	// Updating scores
	GameManager::getInstance().updateUI();

	delete this;
}

// Asteroid moves from upper part of the screen to lower part of the screen
void Asteroid::movement()
{
	Move(0, speed);
	// If asteroid passes end of the screen - place it at the start of the screen at random X position
	if (GetPosition().y >= GameManager::getInstance().getWindow()->getSize().y - getSprite()->getTexture()->getSize().y)
	{
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_real_distribution<> posX(getSprite()->getTexture()->getSize().x,
			GameManager::getInstance().getWindow()->getSize().x - getSprite()->getTexture()->getSize().x);

		SetPosition(posX(gen), 0);
	}
}

// Player's ship

PlayerShip::PlayerShip()
{

}

PlayerShip::~PlayerShip()
{
	for (auto* projectile : projectiles)
	{
		delete projectile;
	}
}

void PlayerShip::takeDamage()
{
	const sf::RenderWindow* window = GameManager::getInstance().getWindow();

	Ship::takeDamage();

	// Send player back to starting position
	SetPosition(window->getSize().x * 0.5f, window->getSize().y - getSprite()->getTexture()->getSize().y - 10);

	// Updating UI
	GameManager::getInstance().updateUI();
}

void PlayerShip::update()
{
	// Checking if player bumps into anything first

	//Enemies
	std::list<EnemyShip*> enemies = GameManager::getInstance().getEnemies();
	std::list<EnemyShip*> enemiesDelete;
	for (auto* enemy : enemies)
	{
		if (getDistanceTo(enemy) < getSprite()->getTexture()->getSize().x)
		{
			enemiesDelete.push_back(enemy);
			takeDamage();
		}
	}
	// Delete enemies
	for (auto* enemy : enemiesDelete)
	{
		enemies.remove(enemy);
		enemy->die();
	}
	enemiesDelete.clear();

	// Asteroids
	std::list<Asteroid*> asteroids = GameManager::getInstance().getAsteroids();
	std::list<Asteroid*> asteroidsDelete;
	for (auto* ast : asteroids)
	{
		if (getDistanceTo(ast) < getSprite()->getTexture()->getSize().x)
		{
			asteroidsDelete.push_back(ast);
			takeDamage();
		}
	}
	// Delete asteroids
	for (auto* ast : asteroidsDelete)
	{
		asteroids.remove(ast);
		ast->die();
	}
	asteroidsDelete.clear();

	// Then we check if out projectiles bump into any object
	std::list<PlayerProjectile*> toDelete;
	for (auto* projectile : projectiles)
	{
		projectile->update();

		// If it's off the screen we don't check for the hit
		if (!projectile->doDelete)
		{
			for (auto* enemy : enemies)
			{
				// Half the size of enemy + half the size of projectile
				if (projectile->getDistanceTo(enemy) < projectile->getSprite()->getTexture()->getSize().y)
				{
					enemy->takeDamage();
					projectile->doDelete = true;
					toDelete.push_back(projectile);
				}
			}
			for (auto* ast : asteroids)
			{
				// Half the size of asteroid + half the size of projectile
				if (projectile->getDistanceTo(ast) < projectile->getSprite()->getTexture()->getSize().y)
				{
					ast->takeDamage();
					projectile->doDelete = true;
					toDelete.push_back(projectile);
				}
			}
		}
		// And just delete it
		else
		{
			toDelete.push_back(projectile);
		}
	}
	// Deleting projectiles
	for (auto* projectile : toDelete)
	{
		projectiles.remove(projectile);
		delete projectile;
	}
}

void PlayerShip::onDeath()
{
	// You die - game over
	GameManager::getInstance().gameState = GameState::GameEnded;
}

void PlayerShip::shoot()
{
	//Creating a new projectile each time we shoot
	PlayerProjectile* projectile = new PlayerProjectile();
	projectile->setTexture(AssetManager::getInstance().getTexture(projectileTexture));
	projectile->SetPosition(GetPosition().x, GetPosition().y - projectile->getSprite()->getTexture()->getSize().y * 2.0f);
	projectile->owner = this;
	projectiles.push_back(projectile);
}