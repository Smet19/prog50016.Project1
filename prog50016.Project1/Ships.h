#pragma once
#ifndef _SHIPS_H_
#define	_SHIPS_H_

#include <string>
#include "Object.h"
#include "Projectile.h"
#include <list>

enum class ShipState
{
	None,
	Alive,
	Dead
};

enum class EnemyMoveDirection
{
	Left,
	Right
};

class Ship : public Object
{
public:
	// Constructors / Destructors
	Ship();
	virtual ~Ship();

	void load(json::JSON& _json) override;
	// Take 1 point of damage
	virtual void takeDamage();
	virtual void shoot() = 0;
	virtual void onDeath() {};
	virtual void die();

	int lives;
	int points;
	float speed;

protected:
	std::string texturePath;
	std::string projectileTexture;
	ShipState status;

};


// Player's ship

class PlayerShip : public Ship
{
public:
	// Constructors / Destructors
	PlayerShip();
	virtual ~PlayerShip();

	void update() override;
	void onDeath() override;
	void shoot() override;
	void takeDamage() override;
	void removeProjectile(PlayerProjectile* _projectile) { projectiles.remove(_projectile); }
	const std::list<PlayerProjectile*>& getProjectiles() { return projectiles; }
private:
	std::list<PlayerProjectile*> projectiles;
};

// Enemies

class EnemyShip : public Ship
{
public:
	// Constructors / Destructors
	EnemyShip();
	virtual ~EnemyShip();

	void update() override;
	void onDeath() override;

	virtual void movement();
	virtual void shoot();
	EnemyProjectile* projectile;
private:
	EnemyMoveDirection direction;
};

class EnemyShipB : public EnemyShip
{
public:
	// Constructors / Destructors
	EnemyShipB() {};
	virtual ~EnemyShipB() {};

	virtual void movement() override;
};

class EnemyShipC : public EnemyShipB
{
public:
	// Constructors / Destructors
	EnemyShipC() {} ;
	virtual ~EnemyShipC() {};
};

// Asteroids
// they kinda are enemy ships, but can't shoot and only move in one direction

class Asteroid : public EnemyShip
{
public:
	// Constructors / Destructors
	Asteroid() {};
	virtual ~Asteroid() {};

	void update() override;
	void onDeath() override;

	virtual void movement() override;
	// Asteroids don't shoot
	void shoot() override { return; }
};

#endif // !_SHIPS_H_