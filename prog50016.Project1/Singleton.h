#pragma once
#ifndef _SINGLETON_H_
#define _SINGLETON_H_

template <typename T>
class Singleton
{
public:
	static T& getInstance()
	{
		static T instance;
		return instance;
	}

protected:
	Singleton() { }
	virtual ~Singleton() { }

private:
	Singleton(Singleton const&) = delete;
	Singleton& operator=(Singleton const&) = delete;
};
#endif // !_SINGLETON_H_
