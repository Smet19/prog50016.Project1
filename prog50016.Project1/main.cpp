#include <iostream>
#include <fstream>
#include <SFML/System.hpp>
#include "SettingsManager.h"
#include "AssetManager.h"
#include "GameManager.h"
#include "Ships.h"
#include <list>

int main()
{
	GameManager::getInstance().initialize();

	if (!GameManager::getInstance().isInitialized())
		return 1;

	// Starting our game 

	while (GameManager::getInstance().getWindow() != nullptr)
	{
		//Gameplay loop
		GameManager::getInstance().gameState = GameState::Gameplay;

		while (GameManager::getInstance().gameState == GameState::Gameplay)
		{
			GameManager::getInstance().update();
		}

		// Post gameplay loop

		if (GameManager::getInstance().getWindow() != nullptr)
		{
			// Updating high score
			if (GameManager::getInstance().getPlayer()->points > GameManager::getInstance().highestScore)
				GameManager::getInstance().highestScore = GameManager::getInstance().getPlayer()->points;
			
			// Waiting for ~5 seconds to comperhend with our loss :)
			for (int i = 0; i < SettingsManager::getInstance().FrameLimit() * 5; i++)
			{
				if (GameManager::getInstance().getWindow() != nullptr)
				{
					GameManager::getInstance().update();
				}
			}

			// Restarting game if window still open
			if (GameManager::getInstance().getWindow() != nullptr) 
				GameManager::getInstance().restart();
		}
	}

	return 0;
}